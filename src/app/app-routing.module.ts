import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ConfirmingOrderApprovalComponent } from './features/confirming-order/approval/confirming-order-approval.component';
import { ConfirmingOrderComponent } from './features/confirming-order/request/confirming-order.component';


const routes: Routes = [
  {path: '', component: ConfirmingOrderComponent},
  {path: 'order-approval', component: ConfirmingOrderApprovalComponent}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
