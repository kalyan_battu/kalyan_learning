import {BrowserModule} from '@angular/platform-browser';
import {NgModule} from '@angular/core';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';

import {AppRoutingModule} from './app-routing.module';
import {AppComponent} from './app.component';
import {UiComponentsModule} from './features/ui-components/ui-components.module';
import {ConfirmingOrderModule} from './features/confirming-order/confirming-order.module';
import {SharedService} from "./shared/shared.service";
import {SpinnerService} from "./shared/spinner.service";
import {SpinnerComponent} from "./shared/spinner.component";
import {SharedModule} from "./shared/shared.module";

@NgModule({
  declarations: [
    AppComponent,
    SpinnerComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    UiComponentsModule,
    ConfirmingOrderModule,
    BrowserAnimationsModule,
    SharedModule
  ],
  bootstrap: [AppComponent],
  providers: [SharedService, SpinnerService]
})
export class AppModule { }
