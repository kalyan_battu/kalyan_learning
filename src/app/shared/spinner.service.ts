import { Injectable, Optional, SkipSelf } from '@angular/core';
import { Observable, Subject } from 'rxjs';

@Injectable()
export class SpinnerService {

    private spinnerSubject = new Subject<boolean>();

    constructor(@Optional() @SkipSelf() spinnerService: SpinnerService) {
        if (!!spinnerService) {
            return spinnerService;
        }
    }

    public show() {
        this.spinnerSubject.next(true);
    }

    public hide() {
        this.spinnerSubject.next(false);
    }

    public getSpinnerSubject(): Observable<any> {
        return this.spinnerSubject.asObservable();
    }
}
