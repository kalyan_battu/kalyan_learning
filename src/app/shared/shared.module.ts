import {NgModule} from '@angular/core';
import {FormsModule} from '@angular/forms';
import {CardModule} from 'primeng/card';
import {InputTextModule} from 'primeng/inputtext';
import {CalendarModule} from 'primeng/calendar';
import {CheckboxModule} from 'primeng/checkbox';
import {CommonModule} from '@angular/common';
import {InputTextareaModule} from 'primeng/inputtextarea';
import {ButtonModule} from 'primeng/button';
import {DropdownModule} from 'primeng/dropdown';
import {MessageModule} from 'primeng/message';
import {RadioButtonModule} from 'primeng/radiobutton';
import {OverlayPanelModule} from 'primeng/overlaypanel';
import {SidebarModule} from 'primeng/sidebar';
import {ToastModule} from 'primeng/toast';
import {MessageService} from 'primeng/api';
import {ProgressSpinnerModule} from 'primeng/progressspinner';
import {SpinnerComponent} from "./spinner.component";
import {DialogModule} from 'primeng/dialog';

@NgModule({
  imports: [
    CardModule,
    InputTextModule,
    FormsModule,
    CalendarModule,
    CheckboxModule,
    CommonModule,
    InputTextareaModule,
    ButtonModule,
    DropdownModule,
    MessageModule,
    RadioButtonModule,
    OverlayPanelModule,
    SidebarModule,
    ToastModule,
    ProgressSpinnerModule,
    DialogModule
  ],
  exports: [
    CardModule,
    InputTextModule,
    FormsModule,
    CalendarModule,
    CheckboxModule,
    CommonModule,
    InputTextareaModule,
    ButtonModule,
    DropdownModule,
    MessageModule,
    RadioButtonModule,
    OverlayPanelModule,
    SidebarModule,
    ToastModule,
    ProgressSpinnerModule,
    DialogModule
  ],
  declarations: [
  ],
  providers: [
    MessageService
  ]
})
export class SharedModule {

}
