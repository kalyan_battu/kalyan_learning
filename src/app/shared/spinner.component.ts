import { Component, OnDestroy, OnInit } from '@angular/core';
import { Subscription } from 'rxjs';
import {SpinnerService} from "./spinner.service";


@Component({
    selector: 'app-spinner',
    template: `
      <div class="app-custom-spinner">
        <p-dialog [modal]="true" [closable]="false" [(visible)]="loading" [draggable]="false" [resizable]="false" [baseZIndex]="100000">
          <p-progressSpinner></p-progressSpinner>
        </p-dialog>
      </div>
    `,
    styles: [`
    `]
})
export class SpinnerComponent implements OnInit, OnDestroy {

    loading = false;
    private spinnerUpdated: Subscription;

    constructor(private spinnerService: SpinnerService) {}

    ngOnInit() {
        this.spinnerUpdated = this.spinnerService.getSpinnerSubject().subscribe(
            (spinnerChange) => this.loading = spinnerChange
        );
    }

    ngOnDestroy() {
      if (!!this.spinnerUpdated) {
        this.spinnerUpdated.unsubscribe();
      }
    }
}
