import {Injectable} from '@angular/core';
import {BehaviorSubject} from 'rxjs';

@Injectable()
export class SharedService {
  booleanFlag: BehaviorSubject<string> = new BehaviorSubject('applicant');
  processInstanceId: BehaviorSubject<string> = new BehaviorSubject('123');
  coApplicantObject: any;
  serverVariables: any;
  currentTask: any;
  currentTaskId: any;
}
