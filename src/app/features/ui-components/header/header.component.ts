import {Component, OnInit} from '@angular/core';
import {SharedService} from '../../../shared/shared.service';
import {SpinnerService} from "../../../shared/spinner.service";
import {Observable} from "rxjs";
import {HttpClient} from "@angular/common/http";
import {environment} from "../../../../environments/environment";
import {finalize} from "rxjs/operators";

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss']
})
export class HeaderComponent implements OnInit {

  constructor(private sharedService: SharedService, private spinnerService: SpinnerService, private httpClient: HttpClient) {}

  visibleSidebar: any;
  enableApproverRoles = false;
  processId: any;

  ngOnInit() {
    this.sharedService.processInstanceId.subscribe( procId => {
      this.enableApproverRoles = (!!procId);
      this.processId = procId;
    });
    this.getVariables();
    this.getTaskInfo();
  }

  updateLoggedUser(user: any) {
    this.sharedService.booleanFlag.next(user);
    this.visibleSidebar = false;
    this.getVariables();
    this.getTaskInfo();
  }

  getVariables() {
    const url = '/runtime/process-instances/processInstanceId/variables';
    if (this.processId != null) {
      this.spinnerService.show();
      this.getData(url.replace('processInstanceId', this.processId))
        .pipe(finalize(() => this.spinnerService.hide()))
        .subscribe((response) => {
            if ((response || []).length > 0) {
              let serverObj = {};
              for (let variableObj of response) {
                serverObj[variableObj.name] = variableObj.value;
              }
              this.sharedService.serverVariables = serverObj;
            } else {
              this.sharedService.serverVariables = [];
            }
        });
    } else {
      this.sharedService.serverVariables = [];
    }
  }

  getTaskInfo() {
    const url = '/activiti-rest/service/runtime/tasks?processInstanceId=processInstanceIdValue';
    if (this.processId != null) {
      this.spinnerService.show();
      this.getData(url.replace('processInstanceIdValue', this.processId))
        .pipe(finalize(() => this.spinnerService.hide()))
        .subscribe((response) => {
          if (!!response && response.data.length > 0) {
            this.sharedService.currentTask = response.data[0].name;
            this.sharedService.currentTaskId = response.data[0].id;
          } else {
            this.sharedService.currentTask = null;
            this.sharedService.currentTaskId = null;
          }
        });
    } else {
      this.sharedService.currentTask = null;
      this.sharedService.currentTaskId = null;
    }
  }

  getData(url): Observable<any> {
    return this.httpClient.get(environment.api + url);
  }
}
