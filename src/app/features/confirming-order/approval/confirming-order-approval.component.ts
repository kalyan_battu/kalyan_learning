import {Component, OnInit, ViewChild} from '@angular/core';
import {SharedService} from '../../../shared/shared.service';
import {HttpClient} from '@angular/common/http';
import {MessageService} from 'primeng/api';
import {finalize} from "rxjs/operators";
import {environment} from "../../../../environments/environment";
import {NgForm} from "@angular/forms";
import {Observable} from "rxjs";
import {SpinnerService} from "../../../shared/spinner.service";

@Component({
  selector: 'app-confirming-order-approval',
  templateUrl: './confirming-order-approval.component.html'
})
export class ConfirmingOrderApprovalComponent implements OnInit{

  constructor(private sharedService: SharedService, private httpClient: HttpClient, private messageService: MessageService, private spinnerService: SpinnerService) {}

  ngOnInit() {
    this.sharedService.booleanFlag.subscribe(value => this.loggedUser = value);

    this.sharedService.booleanFlag.subscribe(value => {
      this.loggedUser = value;
      if (this.loggedUser === 'applicant') {

        this.preloadApplicantData();

        this.setApprovalSections(true, false, false);
        this.setFieldsDisbaled(true, true, true);
      } else if (this.loggedUser === 'unitApprover') {

        this.preloadApplicantData();
        this.preloadUnitApproverData();

        this.setApprovalSections(true, false, false);
        this.setFieldsDisbaled(true, false, false);
      } else if (this.loggedUser === 'bocApprover') {

        this.preloadApplicantData();
        this.preloadUnitApproverData();
        this.preloadBocApproverData();

        this.setApprovalSections(true, true, false);
        this.setFieldsDisbaled(true, false, false);
      } else if (this.loggedUser === 'controller') {

        this.preloadApplicantData();
        this.preloadUnitApproverData();
        this.preloadBocApproverData();
        this.preloadControllerData();

        this.setApprovalSections(true, true, true);
        this.setFieldsDisbaled(true, true, false);
      }
    });

    if (this.sharedService.serverVariables != null) {
      const serverVars = this.sharedService.serverVariables;
      this.selectedUnitApprover = serverVars.selectedUnitApprover;
      this.selectedBocApprover = serverVars.selectedBocApprover;
      this.unitApprovedDate = serverVars.unitApprovedDate;
      this.unitApproverEmail= serverVars.unitApproverEmail;
      this.unitApproverComments = serverVars.unitApproverComments;
      this.bocApprovedDate = serverVars.bocApprovedDate;
      this.bocApproverEmail = serverVars.bocApproverEmail;
      this.bocApproverComments = serverVars.bocApproverComments;
      this.controllerApprovedDate = serverVars.controllerApprovedDate;
      this.controllerApproverEmail = serverVars.controllerApproverEmail;
      this.controllerApproverComments = serverVars.controllerApproverComments;
    }
  }

  unitApprovers = [
    {label: 'Unit Approver 1', value: 'unit1'},
    {label: 'Unit Approver 2', value: 'unit2'},
    {label: 'Unit Approver 3', value: 'unit3'},
    {label: 'Unit Approver 4', value: 'unit4'},
  ];
  bocApprovers = [
    {label: 'BOC Approver 1', value: 'boc1'},
    {label: 'BOC Approver 2', value: 'boc2'},
    {label: 'BOC Approver 3', value: 'boc3'},
    {label: 'BOC Approver 4', value: 'boc4'},
  ];

  selectedUnitApprover: string;
  selectedBocApprover: string;
  unitApprovedDate: any;
  unitApproverEmail: string;
  unitApproverComments: string;

  bocApprovedDate: any;
  bocApproverEmail: string;
  bocApproverComments: string;

  controllerApprovedDate: any;
  controllerApproverEmail = 'controllerApprover@testmail.com';
  controllerApproverComments: string;

  unitApproverAction: any;
  bocApproverAction: any;
  controllerApproverAction: any;
  loggedUser: any;

  unitApprovalFieldsetDisabled = false;
  bocApprovalFieldsetDisabled = false;
  controllerFieldsetDisabled = false;

  showUnitApproval = true;
  showBocApproval = false;
  showControllerApproval = false;

  formSubmitted = false;
  @ViewChild('applicantForm', {static: false}) applicantForm: NgForm;
  @ViewChild('bocForm', {static: false}) bocForm: NgForm;
  @ViewChild('controllerForm', {static: false}) controllerForm: NgForm;

  preloadData() {

    if (this.loggedUser === 'applicant') {
      this.preloadApplicantData();
    } else if (this.loggedUser === 'unitApprover') {
      this.preloadUnitApproverData();
    } else if (this.loggedUser === 'bocApprover') {
      this.preloadBocApproverData();
    } else if (this.loggedUser === 'controller') {
      this.preloadControllerData();
    }
  }

  preloadApplicantData() {
    this.selectedUnitApprover = 'unit1';
    this.selectedBocApprover = 'boc1';
    this.unitApprovedDate = new Date();
    this.unitApproverEmail = 'unitApprover@testmail.com';
  }

  preloadUnitApproverData() {
    this.unitApproverComments = 'Sample Unit Approver Comments';
    this.unitApproverAction = 'APPROVE';
  }

  preloadBocApproverData() {
    this.bocApprovedDate = new Date();
    this.bocApproverEmail = 'bocApprover1@testmail.com';
    this.bocApproverComments = 'Sample BOC Approver Comments';
    this.bocApproverAction = 'APPROVE';
  }

  preloadControllerData() {
    this.controllerApprovedDate = new Date();
    this.controllerApproverComments = 'Sample Controller Comments';
    this.controllerApproverAction = 'APPROVE';
  }

  clearApplicantData() {
    this.selectedUnitApprover = null;
    this.selectedBocApprover = null;
    this.unitApprovedDate = null;
    this.unitApproverEmail = null;
  }

  clearUnitApproverData() {
    this.unitApproverComments = null;
    this.unitApproverAction = null;
  }

  clearBocApproverData() {
    this.bocApprovedDate = null;
    this.bocApproverEmail = null;
    this.bocApproverComments = null;

    this.bocApproverAction = null;
  }

  clearControllerData() {
    this.controllerApprovedDate = null;
    this.controllerApproverComments = null;

    this.controllerApproverAction = null;
  }

  clearPreloadedData() {
    if (this.loggedUser === 'applicant') {
      this.clearApplicantData();
    } else if (this.loggedUser === 'unitApprover') {
      this.clearUnitApproverData();
    } else if (this.loggedUser === 'bocApprover') {
      this.clearBocApproverData();
    } else if (this.loggedUser === 'controller') {
      this.clearControllerData();
    }
  }

  clearPreloadedDataForAll() {
    this.selectedUnitApprover = '';
    this.selectedBocApprover = '';
    this.unitApprovedDate = null;
    this.unitApproverEmail = '';
    this.unitApproverComments = '';

    this.bocApprovedDate = null;
    this.bocApproverEmail = '';
    this.bocApproverComments = '';

    this.controllerApprovedDate = null;
    this.controllerApproverComments = '';

    this.unitApproverAction = '';
    this.bocApproverAction = '';
    this.controllerApproverAction = '';
  }

  setApprovalSections(unitApp, bocAppr, cont) {
    this.showUnitApproval = unitApp;
    this.showBocApproval = bocAppr;
    this.showControllerApproval = cont;
  }

  setFieldsDisbaled(unitApp, bocAppr, cont) {
    this.unitApprovalFieldsetDisabled = unitApp;
    this.bocApprovalFieldsetDisabled = bocAppr;
    this.controllerFieldsetDisabled = cont;
  }

  submitCo() {
    let body, url;
    this.formSubmitted = true;
    if (this.loggedUser === 'applicant') {

      if (this.applicantForm.invalid) {
        return false;
      } else {

        let coApplicantObject = this.sharedService.coApplicantObject;
        if (!coApplicantObject) {
          coApplicantObject = {
            vendorName:'Vendor ABCD',
            purchaseDate:new Date(),
            amountPaid: 40.65,
            purchaseType: 'pcType1',
            businessProgram:'Sample Business Program',
            reasonForUnAuthPurchase: 'Sample Reason for Unauthorized purchase',
          };
        }

        const applicantUnitApproverApplicant = {
          selectedUnitApprover: this.selectedUnitApprover,
          unitApprovedDate: this.unitApprovedDate,
          selectedBocApprover: this.selectedBocApprover,
          unitApproverEmail: this.unitApproverEmail
        };
        this.sharedService.coApplicantObject = {...coApplicantObject, ...applicantUnitApproverApplicant};

        const variables = [];
        for (var key in this.sharedService.coApplicantObject) {
          if (this.sharedService.coApplicantObject.hasOwnProperty(key)) {
            variables.push({name: key, value: this.sharedService.coApplicantObject[key]});
          }
        }

        body = {
          "processDefinitionId": "OrderManagement:16:15436",
          "businessKey": "OrderManagement:16:15436",
          "variables": variables
        };
        url = '/activiti-rest/service/runtime/process-instances';

      }
    } else if (this.loggedUser === 'unitApprover') {
      url = '/activiti-rest/service/runtime/tasks/currentTaskId'.replace('currentTaskId', this.sharedService.currentTaskId);


        body = {
          unitApproverComments: this.unitApproverComments,
          unitApproverAction: this.unitApproverAction
        };
    } else if (this.loggedUser === 'bocApprover') {
      url = '/activiti-rest/service/runtime/tasks/currentTaskId'.replace('currentTaskId', this.sharedService.currentTaskId);

      if (this.bocForm.invalid) {
        return false;
      } else {

        body = {
          bocApprovedDate: this.bocApprovedDate,
          bocApproverEmail: this.bocApproverEmail,
          bocApproverComments: this.bocApproverComments,
          bocApproverAction: this.bocApproverAction
        };
      }
    } else if (this.loggedUser === 'controller') {
      url = '/activiti-rest/service/runtime/tasks/currentTaskId'.replace('currentTaskId', this.sharedService.currentTaskId);
      if (this.controllerForm.invalid) {
        return false;
      } else {
        body = {
          controllerApprovedDate: this.controllerApprovedDate,
          controllerApproverComments: this.controllerApproverComments,
          controllerApproverAction: this.controllerApproverAction
        };
      }
    }

    this.spinnerService.show();
    this.submit(environment.api + url, body)
      .pipe( finalize(()=> {
        this.showMessage();
        this.spinnerService.hide();
      }))
      .subscribe((response) => {

        if (!!response) {
          this.sharedService.processInstanceId = response.id;
        }

      },
      error => {

      });
  }

  submit(url, body): Observable<any> {
    return this.httpClient.post(url, body);
  }

  setUnitEmail() {
    this.unitApproverEmail = 'unitApprover@testmail.com';
  }

  showMessage() {
    let message;
    if (this.loggedUser === 'applicant') {
      message = 'Request submitted successfully';
    } else if (this.loggedUser === 'unitApprover') {

      if (this.unitApproverAction === 'APPROVE') {
        message = 'Unit Approvals completed successfully';
      } else {
        message = 'Unit Rejection completed successfully';
      }

    } else if (this.loggedUser === 'bocApprover') {

      if (this.bocApproverAction === 'APPROVE') {
        message = 'BOC Approvals completed successfully';
      } else {
        message = 'BOC Rejection completed successfully';
      }
    } else if (this.loggedUser === 'controller') {

      if (this.controllerApproverAction === 'APPROVE') {
        message = 'Controller Approvals completed successfully';
      } else {
        message = 'Controller Rejection completed successfully';
      }
    }
    this.messageService.add({severity:'success', summary:'Server Response', detail:message});
  }
}
