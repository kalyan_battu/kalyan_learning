import {Component, OnInit} from '@angular/core';
import {SharedService} from '../../../shared/shared.service';
import {Router} from "@angular/router";

@Component({
  selector: 'app-confirming-order-request',
  templateUrl: './confirming-order.component.html',
  styleUrls: ['./confirming-order.component.scss']
})
export class ConfirmingOrderComponent implements OnInit {

  constructor(private sharedService: SharedService, private router: Router) {}

  ngOnInit() {
    this.sharedService.booleanFlag.subscribe(value => {
      this.loggedUser = value;
      // this.preloadData();
      if (this.loggedUser !== 'applicant') {
        this.fieldsetDisabled = true;
      } else {
        this.fieldsetDisabled = false;
      }
    });

    if (this.sharedService.serverVariables != null) {
      const serverVars = this.sharedService.serverVariables;
      this.vendorName = serverVars.vendorName;
      this.purchaseDate = serverVars.purchaseDate;
      this.amountPaid = serverVars.amountPaid;
      this.purchaseType= serverVars.purchaseType;
      this.description = serverVars.description;
      this.businessProgram = serverVars.businessProgram;
      this.reasonForUnAuthPurchase = serverVars.reasonForUnAuthPurchase;
    }
  }

  vendorName: string;
  purchaseDate: Date;
  amountPaid: number;
  purchaseType: any;
  description: string;
  businessProgram: string;
  reasonForUnAuthPurchase: string;
  loggedUser: string;
  fieldsetDisabled = false;

  options: any[] = [
    {label: 'Reimbursement for employee / volunteer out of pocket expenses $500 or more per transaction', value: 'pcType1'},
    {label: 'Unauthorized purchase without an approved UC purchase order', value: 'pcType2'},
    {label: 'Unauthorized service without an approved UC business contract', value: 'pcType3'},
    {label: 'Other (Please describe)', value: 'pcType4'}
  ];

  preloadData() {
    this.vendorName = 'Vendor ABCD';
    this.purchaseDate = new Date();
    this.amountPaid = 40.65;
    this.purchaseType = 'pcType1';
    this.businessProgram = 'Sample Business Program';
    this.reasonForUnAuthPurchase = 'Sample Reason for Unauthorized purchase';
  }

  clearPreloadedData() {
    this.vendorName = null;
    this.purchaseDate = null;
    this.amountPaid = null;
    this.description = null;
    this.purchaseType = null;
    this.businessProgram = '';
    this.reasonForUnAuthPurchase = null;
  }

  gotoNextPage() {
    this.sharedService.coApplicantObject = {
      vendorName: this.vendorName,
      purchaseDate: this.purchaseDate,
      amountPaid: this.amountPaid,
      purchaseType: this.purchaseType,
      businessProgram: this.businessProgram,
      reasonForUnAuthPurchase: this.reasonForUnAuthPurchase
    };
    this.router.navigate(['/order-approval']);
  }
}
