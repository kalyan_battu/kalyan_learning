import {NgModule} from '@angular/core';
import {ConfirmingOrderComponent} from './request/confirming-order.component';
import {SharedModule} from '../../shared/shared.module';
import {ConfirmingOrderApprovalComponent} from './approval/confirming-order-approval.component';
import {RouterModule} from '@angular/router';
import {HttpClientModule} from '@angular/common/http';

@NgModule({
  imports: [
    SharedModule,
    RouterModule,
    HttpClientModule
  ],
  exports: [
    ConfirmingOrderComponent,
    ConfirmingOrderApprovalComponent
  ],
  declarations: [
    ConfirmingOrderComponent,
    ConfirmingOrderApprovalComponent
  ],
  providers: [

  ]
})
export class ConfirmingOrderModule {

}
